<!-- <footer>
    <div class="container">
        <div class="row" style="padding: 4em">

            <div class="col-md-3">
                <p><strong>Technology Integration</strong></p>
                <p><a href="https://secure.my10yearplan.com">My10YearPlan</a></p>
                <p><a>Career Choices</a></p>
                <p><a>Lifestyle Math</a></p>
                <p><a>Get Focused, Stay Focused</a></p>
                <p><a>Standards Alignment</a></p>
                <p><a>Online Enhancements</a></p>
                <p><a>Membership Information</a></p>
                <br>
                <p><strong>Order Forms</strong></p>
                <p><a>Request a Review Set</a></p>
            </div>
            <div class="col-md-3">
                <p><strong>Professional Development</strong></p>
                <p><a>Online Program Planning Tools</a></p>
                <p><a>Professional Development</a></p>
                    <br>
                <p><strong>Workshops</strong></p>
                <p><a>Upcoming Workshops</a></p>
                <p><a>Focus on Freshmen Conference</a></p>
                <p><a>Co-Host a Workshop</a></p>
                <p><a>Onsite Workshops</a></p>
                <p><a>Online Training Videos</a></p>
                <p><a>Technology Integration Workshops</a></p>
                <p><a>Workshop Reviews</a></p>
            </div>
            <div class="col-md-3">
                <p><strong>Curriculum Support</strong></p>
                <p><a>The Teachers' Lounge</a></p>
                <p><a>Sample Lesson Plans</a></p>
                <p><a>Funding</a></p>
                <p><a>Interviews with Teachers</a></p>
                <p><a>Who is Using it & How</a></p>
                <p><a>Program Rubric</a></p>
                <p><a>Classroom Ideas and Tips</a></p>
                <p><a>Online Newsletter</a></p>
                <p><a>Teacher Survey Form</a></p>
            </div>
            <div class="col-md-3">
                <p><strong>Contact Us</strong></p>
                <p><a>Staff & Consultants</a></p>
                <p><a>Provide Feedback</a></p>
                <p><a>Media Center</a></p>
                <p><a>Company Overview</a></p>
                <p><a>Work for Us</a></p>
                    <br>
                <p><strong>Curriculum Overview</strong></p>
                <p><a>Awards and Evaluations</a></p>
                <p><a>Data and Proven Programs</a></p>
                <p><a>Research & Data</a></p>
                <p><a>Overview of College Text</a></p>
            </div>

            <div class="col-md-12">
                <p style="padding-top: 3em; text-align:center;">
                    &copy; 2019 Academic Innovations LLC 59 South 100 East, Saint George, UT 84770 Toll-Free (800) 967-8016
                </p>
            </div>
            

        </div>
    </div>
</footer> -->

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/mobile-nav/mobile-nav.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/isotope/isotope.pkgd.min.js"></script>
    <script src="lib/lightbox/js/lightbox.min.js"></script>


	<!-- <script src="js/script.js"></script> -->
	<!-- <script src="js/test.js"></script> -->


</div>	
</body>
</html>